package test.emulator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Дто с информацией о датчике температуры.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TemperatureSensor {

    /**
     * Идентификатор датчика температуры. */
    @JsonProperty("id")
    private Integer id;

    /**
     * Показание температуры. */
    @JsonProperty(value = "temperature")
    private Double temperature;
}
