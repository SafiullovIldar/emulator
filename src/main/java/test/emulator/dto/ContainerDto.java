package test.emulator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import test.emulator.dto.enums.SensorType;

/**
 * Дто с информацией о контейнере.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContainerDto {

    /**
     * Идентификатор контейнера. */
    @JsonProperty("id")
    private Integer id;

    /**
     * Датчик температуры. */
    @JsonProperty("temperature_sensor")
    private TemperatureSensor temperatureSensor;

    /**
     * Датчик удара. */
    @JsonProperty("shock_sensor")
    private ShockSensor shockSensor;

    /**
     * Датчик влажности. */
    @JsonProperty("humidity_sensor")
    private HumiditySensorDto humiditySensorDto;

    /**
     * Тип датчика. */
    @JsonProperty("sensor_sensor")
    private SensorType sensorType;

    public ContainerDto(Integer id, TemperatureSensor temperatureSensor, SensorType sensorType) {
        this.id = id;
        this.temperatureSensor = temperatureSensor;
        this.sensorType = sensorType;
    }

    public ContainerDto(Integer id, ShockSensor shockSensor, SensorType sensorType) {
        this.id = id;
        this.shockSensor = shockSensor;
        this.sensorType = sensorType;
    }

    public ContainerDto(Integer id, HumiditySensorDto humiditySensorDto, SensorType sensorType) {
        this.id = id;
        this.humiditySensorDto = humiditySensorDto;
        this.sensorType = sensorType;
    }
}
