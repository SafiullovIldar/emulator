package test.emulator.dto.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 * Тип датчика.
 */
@AllArgsConstructor
public enum SensorType {

    /**
     * Датчика температуры. */
    TEMPERATURE("TEMPERATURE"),

    /**
     * Датчик удара. */
    SHOCK("SHOCK"),

    /**
     * Датчик влажности. */
    HUMIDITY("HUMIDITY");

    /**
     * Строковое представление текущей enum-константы (тип датчика),
     */
    @NonNull
    private final String value;

    /**
     * @return строковое представление текущей enum-константы.
     */
    @JsonValue
    public String getValue() {
        return value;
    }
}
