package test.emulator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Дто с информацией о датчике удара.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShockSensor {

    /**
     * Идентификатор датчика удара. */
    @JsonProperty("id")
    private Integer id;

    /**
     * Информация о повреждении. */
    @JsonProperty(value = "is_damaged")
    private Boolean isDamaged;
}
