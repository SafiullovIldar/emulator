package test.emulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@PropertySource(value = {
	"classpath:custom.properties",
	"classpath:application.properties"}, encoding = "UTF-8")
@SpringBootApplication
@EnableScheduling
public class EmulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmulatorApplication.class, args);
		System.out.println();
	}

}
