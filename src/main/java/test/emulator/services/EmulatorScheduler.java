package test.emulator.services;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import test.emulator.dto.ContainerDto;
import test.emulator.dto.HumiditySensorDto;
import test.emulator.dto.ShockSensor;
import test.emulator.dto.TemperatureSensor;
import test.emulator.dto.enums.SensorType;
import test.emulator.services.exception.UnsupportedSensorTypeException;
import test.emulator.util.RandomValueGenerator;

@Component
@RequiredArgsConstructor
public class EmulatorScheduler {

    /**
     * Выводит информацию в лог. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmulatorScheduler.class);

    /**
     * Класс для отправки REST - запросов. */
    @NonNull private final RestTemplate restTemplate;

    @Value("${url.requestManager.sendNewIndication}")
    private String url;

    @Scheduled(fixedRateString = "${scheduler.fixedRateValue}")
    public void sendNewSensorsValues() {
        final Integer containerAndSensorId = RandomValueGenerator.getRandomInt();
        final SensorType type = RandomValueGenerator.getRandomEnum(SensorType.class);
        final ContainerDto containerDto = getContainer(containerAndSensorId, type);
        restTemplate.postForEntity(url, containerDto, Void.class);
        LOGGER.info("Отправлено sensor request manager " + containerDto);
    }

    /**
     * @param containerAndSensorId идентификатор контейнра и его датчиков.
     * @param type тип датчика
     * @return дто с информацией контейнере и случайно сгенерированном датчке.
     */
    private ContainerDto getContainer(@NonNull final Integer containerAndSensorId, @NonNull final SensorType type) {
        final ContainerDto containerDto;
        switch (type) {
            case TEMPERATURE:
                final Double temperature = RandomValueGenerator.randomTemperature();
                containerDto = new ContainerDto(
                    containerAndSensorId,
                    new TemperatureSensor(containerAndSensorId, temperature),
                    SensorType.TEMPERATURE
                );
                break;
            case SHOCK:
                final Boolean isDamaged = RandomValueGenerator.getRandomBoolean();
                containerDto = new ContainerDto(
                    containerAndSensorId,
                    new ShockSensor(containerAndSensorId, isDamaged),
                    SensorType.SHOCK
                );
                break;
            case HUMIDITY:
                final Integer randomHumidity = RandomValueGenerator.getRandomHumidity();
                containerDto = new ContainerDto(
                    containerAndSensorId,
                    new HumiditySensorDto(containerAndSensorId, randomHumidity),
                    SensorType.HUMIDITY
                );
                break;
            default: throw new UnsupportedSensorTypeException("Не поддерживаемый тип датчика.");
        }

        return containerDto;
    }
}
