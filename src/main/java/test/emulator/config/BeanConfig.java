package test.emulator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Класс для конфигурации бинов.
 */
@Configuration
public class BeanConfig {

    /**
     * @return бин для отправки rest - запросов. */
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
