package test.emulator.util;

import lombok.NonNull;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Класс, который генерирует рандомные значения.
 */
public final class RandomValueGenerator {

    /**
     * Генератор случайных чисел. */
    private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();

    /**
     * @return случайный Integer.
     */
    public static Integer getRandomInt() {
        final int min = 1;
        final int max = 6;
        return Math.abs(RANDOM.nextInt(min, max));
    }

    /**
     * @return случайный Boolean.
     */
    public static Boolean getRandomBoolean() {
        return RANDOM.nextBoolean();
    }

    /**
     * @return случайная влажность.
     */
    public static Integer getRandomHumidity() {
        final int min = 0;
        final int max = 100;
        return Math.abs(RANDOM.nextInt(min, max));
    }

    /**
     * @return случайная температура.
     */
    public static Double randomTemperature() {
        final Double abs = Math.abs(RANDOM.nextDouble(-50.0, 100.0)) - 50.0;
        final DecimalFormat df = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.ENGLISH));
        return Double.parseDouble(df.format(abs));
    }

    /**
     * @param enumClass класс соотвествующий enum типу.
     * @param <T> тип.
     * @return случайный enum.
     */
    public static <T extends Enum<T>> T getRandomEnum(@NonNull final Class<T> enumClass) {
        int i = getRandomAbsIntTo(enumClass.getEnumConstants().length);
        return enumClass.getEnumConstants()[i];
    }

    /**
     * @param max максимальное значение.
     * @return случайный Integer от 0 до значения max.
     */
    public static Integer getRandomAbsIntTo(final Integer max) {
        return RANDOM.nextInt(max);
    }
}
